-- Mariel Genodiala BSIT- 3

-- a Find all artists that has letter D in its name.
SELECT * FROM artists WHERE name LIKE "%D%";

--b Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

--c Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

--d Join the 'artists' and 'albums' tables. (Find all albums that has letter A in its name.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE "%A%";

--e Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--f Join the 'albums' and 'songs' tables. (Sort albums from Z-A and sort songs from A-Z.)
SELECT albums.*, songs.* FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY albums.album_title DESC, songs.song_name ASC;